mod entity;
mod interface;
mod item;
mod rng;
mod stats;
mod world;
use entity::EntityInteractors;

fn main() {
    /*     let power = stats::AttackPower {
        min: 1,
        max: 100,
        damage_type: stats::DamageType::Physical(stats::PhysicalDamageType::Piercing),
    };
    println!("{}", power.gen_damage().amount);
    let mut healthh = stats::Health {
        current: 25,
        max: 50,
    };
    println!("current health: {}", healthh.current);
    let heal = healthh.add(30);
    println!(
        "you healed {} ({} overheal)! current health: {} / {}",
        heal.amount, heal.overheal, healthh.current, healthh.max
    );
    let subtract = healthh.sub(15);
    println!(
        "you dealt {} damage ({} overkill)! current health: {} / {}",
        subtract.amount, subtract.overkill, healthh.current, healthh.max
    );
    let heal = healthh.add(500);
    println!(
        "you healed {} ({} overheal)! current health: {} / {}",
        heal.amount, heal.overheal, healthh.current, healthh.max
    );
    let subtract = healthh.sub(45);
    println!(
        "you dealt {} damage ({} overkill)! current health: {} / {}",
        subtract.amount, subtract.overkill, healthh.current, healthh.max
    );
    let restore = healthh.restore();
    println!(
        "you restore {}! current health: {} / {}",
        restore.amount, healthh.current, healthh.max
    );
    let subtract = healthh.sub(28);
    println!(
        "you dealt {} damage ({} overkill)! current health: {} / {}",
        subtract.amount, subtract.overkill, healthh.current, healthh.max
    );
    let drained = healthh.drain();
    println!(
        "you drain {}! current health: {} / {}",
        drained.amount, healthh.current, healthh.max
    );
    println!("{}", healthh);

    let mut slime = entity::EnemyKind::Skeleton.into_enemy();

    //println!("{:#?}", slime);

    let mut inventory_weapon =
        item::InvItem::Weapon(item::WeaponKind::Katana.into_weapon().unwrap());
    println!("\n{}", inventory_weapon.tooltip());

    let mut inventory_weapon_two = item::WeaponKind::Katana.into_weapon().unwrap();
    println!("\n{}", inventory_weapon_two.tooltip());

    let statss = stats::Attribute::new(Some(15), Some(50), Some(8), Some(5), Some(5));
    println!("{}", statss.gen_level());

    println!(
        "sufficent?: {}\nstats 1: {:?}\nstats 2: {:?}",
        stats::attribute_is_sufficent(&statss, &inventory_weapon_two.requirement),
        &statss,
        &inventory_weapon_two.requirement
    );

    let resistancee = stats::Resistance {
        intensity: -55,
        damage_type: stats::DamageType::Physical(stats::PhysicalDamageType::Bludgeoning),
    };
    println!("{:#?}\n{}", resistancee, resistancee.into_string());

    let robe = item::ArmorKind::Robe.into_armor();
    println!(
        "ResistanceDebug\n{}",
        robe.unwrap().resistances_to_string().unwrap()
    ) */
    let skeleton = entity::EnemyKind::Skeleton.into_enemy();
    let mut player = entity::Player {
        name: "MtotheM",
        health: stats::Health::new(10),
        weapon: item::WeaponKind::Dagger.into_weapon(),
        armor: item::ArmorKind::Robe.into_armor(),
        attributes: stats::Attribute {
            strength: Some(5),
            dexterity: Some(5),
            vitality: Some(5),
            energy: Some(5),
            magic: Some(5),
        },
        money: stats::Money::new(53),
        class: entity::StartClass::Deprived,
        inventory: vec![item::InvItem::Armor(
            item::ArmorKind::FullPlate.into_armor().unwrap(),
        )],
    };
    println!("{:#?}", player);
    //let plate = item::InvItem::Armor(item::ArmorKind::FullPlate.into_armor().unwrap());
    //let item_test = player.inventory.remove(0);
    let check = player.equip_from_inventory(1);
    println!("{:#?}", player);
    println!("Equip were {}", {
        match check {
            Ok(_) => "Sucessful! :D".to_string(),
            Err(e) => format!("Unsucessful :( Reason: {}", e),
        }
    });
    let start = interface::StartScreen { about: "Welcome to Rustcrawler! This is a small game that let's you explore dungeons in order to progress. Your goal as the player is to reach the last dungeon and defeat the Demon Lord. Dying is not permanent, but you will lose resources and any progress. however once you have cleared a dungeon it will stay cleared." };
    println!("\n{}", start.about);

    //let temporary = item::Items { 
    //    weapons: vec![item::WeaponKind::Katana.into_weapon().unwrap()], 
    ////    armors: vec![item::ArmorKind::Robe.into_armor().unwrap()], 
    //    //consumable: vec![item::Consumable::from(item::ConsumableKind::Potion(item::PotionKind::Healing))] 
    //    };
}
