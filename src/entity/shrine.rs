use crate::world{ Location }

struct Shrine {
    location: Location,
    desc_inspect: &'static str,
    desc_use: &'static str,
}

impl Shrine {
    pub fn description(&self) -> &'static str {
        &self.desc_inspect
    }
    pub fn use(&self) -> &'static str {
        &self.desc_use
    }
}