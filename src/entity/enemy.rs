use crate::item::{Armor, ArmorKind, Weapon, WeaponKind}; // imports required item types required by entity.
use crate::stats::{
    Attribute, DamageType, Health, MagicDamageType, Money, Resistance,
}; // imports required stats types required by entity.

#[derive(Debug)]
/// Holds information about an enemy.
pub struct Enemy {
    pub name: &'static str,
    pub health: Health,
    pub attributes: Attribute,
    pub weapon: Option<Weapon>,
    pub armor: Option<Armor>,
    pub money: Money,
    pub kind: EnemyKind,
    pub resistances: Vec<Resistance>,
}

#[derive(Debug)]
/// Various kinds of enemy.
pub enum EnemyKind {
    Skeleton,
    Slime,
}

impl EnemyKind {
    /// Assembles an Enemy struct from a EnemyKind enum.
    pub fn into_enemy(&self) -> Enemy {
        match self {
            EnemyKind::Skeleton => Enemy {
                name: "Skeleton",
                health: Health::new(15),
                attributes: Attribute {
                    strength: Some(5),
                    dexterity: Some(5),
                    vitality: Some(5),
                    energy: Some(5),
                    magic: Some(5),
                },
                weapon: WeaponKind::Katana.into_weapon(),
                armor: ArmorKind::FullPlate.into_armor(),
                kind: EnemyKind::Skeleton,
                money: Money::new_range(15, 30),
                resistances: vec![Resistance::new(
                    32,
                    DamageType::Magic(MagicDamageType::Fire),
                )],
            },
            EnemyKind::Slime => Enemy {
                name: "Slime",
                health: Health::new(60),
                attributes: Attribute {
                    strength: Some(5),
                    dexterity: Some(5),
                    vitality: Some(5),
                    energy: Some(5),
                    magic: Some(5),
                },
                weapon: None,
                armor: None,
                money: Money::new_range(30, 50),
                kind: EnemyKind::Slime,
                resistances: vec![Resistance::new(
                    32,
                    DamageType::Magic(MagicDamageType::Fire),
                )],
            },
        }
    }
}
