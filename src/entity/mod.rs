mod class;
mod enemy;
mod player;
pub use class::StartClass;
pub use enemy::*;
pub use player::*;

use crate::item::InvItem;

/// Implements condition checks for entities.
trait EntityConditions {
    fn is_dead() -> bool;
    fn is_hostile() -> bool;
    fn is_naked() -> bool;
    fn is_unarmed() -> bool;
    //fn is_weak_to_damage_type(DamageType) -> bool;
    //fn is_resistant_to_damage_type(DamageType) -> bool;
    //fn is_weak_or_resist_to_damage_type(DamageType) -> bool;
}

trait EntityGetters {
    fn get_name() -> String;
}

pub trait EntityInteractors {
    fn equip(&mut self, item: InvItem) -> Result<(), &'static str>;
    fn equip_from_inventory(&mut self, index: usize) -> Result<(), &'static str>;
    //fn unequip_weapon() -> InvItem;
    //fn unequip_armor() -> InvItem;
}