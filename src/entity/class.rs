use crate::entity::Player;
use crate::item::{ArmorKind, WeaponKind};
use crate::stats::{Attribute, Health, Money};

#[derive(Debug)]
pub enum StartClass {
    Knight,
    Warrior,
    Mage,
    Rogue,
    Deprived,
}

impl StartClass {
    /// builds the player struct based on a start class.
    fn into_player(self, name: &'static str) -> Player {
        match self {
            StartClass::Knight => Player {
                name,
                class: self,
                health: Health::new(10),
                attributes: Attribute {
                    strength: Some(5),
                    dexterity: Some(5),
                    vitality: Some(5),
                    energy: Some(5),
                    magic: Some(5),
                },
                weapon: WeaponKind::Katana.into_weapon(),
                armor: ArmorKind::ChainMail.into_armor(),
                money: Money::new(15),
                inventory: Vec::new(),
            },
            StartClass::Warrior => Player {
                name,
                class: self,
                health: Health::new(10),
                attributes: Attribute {
                    strength: Some(5),
                    dexterity: Some(5),
                    vitality: Some(5),
                    energy: Some(5),
                    magic: Some(5),
                },
                weapon: WeaponKind::Katana.into_weapon(),
                armor: ArmorKind::ChainMail.into_armor(),
                money: Money::new(15),
                inventory: Vec::new(),
            },
            StartClass::Mage => Player {
                name,
                class: self,
                health: Health::new(10),
                attributes: Attribute {
                    strength: Some(5),
                    dexterity: Some(5),
                    vitality: Some(5),
                    energy: Some(5),
                    magic: Some(5),
                },
                weapon: WeaponKind::Katana.into_weapon(),
                armor: ArmorKind::ChainMail.into_armor(),
                money: Money::new(15),
                inventory: Vec::new(),
            },
            StartClass::Rogue => Player {
                name,
                class: self,
                health: Health::new(10),
                attributes: Attribute {
                    strength: Some(5),
                    dexterity: Some(5),
                    vitality: Some(5),
                    energy: Some(5),
                    magic: Some(5),
                },
                weapon: WeaponKind::Katana.into_weapon(),
                armor: ArmorKind::ChainMail.into_armor(),
                money: Money::new(15),
                inventory: Vec::new(),
            },
            StartClass::Deprived => Player {
                name,
                class: self,
                health: Health::new(5),
                attributes: Attribute {
                    strength: Some(5),
                    dexterity: Some(5),
                    vitality: Some(5),
                    energy: Some(5),
                    magic: Some(5),
                },
                weapon: None,
                armor: None,
                money: Money::new(0),
                inventory: Vec::new(),
            },
        }
    }
}