use crate::entity::StartClass;
use crate::item::{Armor, InvItem, Item, Weapon}; // imports required item types required by entity.
use crate::stats::{
    Attribute, Health, Money,
}; // imports required stats types required by entity.
use super::{EntityInteractors};

#[derive(Debug)]
/// Holds information about the player.
pub struct Player {
    pub name: &'static str,
    pub class: StartClass,
    pub health: Health,
    pub attributes: Attribute,
    pub weapon: Option<Weapon>,
    pub armor: Option<Armor>,
    pub money: Money,
    pub inventory: Vec<InvItem>,
}

impl EntityInteractors for Player {
    /// Equips an inventory item to the player. and returns the currently equipped one to inventory (if any).
    fn equip(&mut self, item: InvItem) -> Result<(), &'static str> {
        match item {
            InvItem::Weapon(weapon) => {
                if let Some(unequipped) = self.weapon.replace(weapon) {
                    let unequipped = unequipped.into_inv_item();
                    self.inventory.push(unequipped);
                }
                Ok(())
            }
            InvItem::Armor(armor) => {
                if let Some(unequipped) = self.armor.replace(armor) {
                    let unequipped = unequipped.into_inv_item();
                    self.inventory.push(unequipped);
                }
                Ok(())
            }
            _ => Err("Error: Tried to equip an unequippable item!"),
        }
    }
    fn equip_from_inventory(&mut self, index: usize) -> Result<(), &'static str> {
        if self.inventory.len() > index {
            let removed = self.inventory.remove(index);
            self.equip(removed);
        } else {
            return Err("Error: The item you tried to equip does not exist!");
        }
        Ok(())
    }
}