mod serialize;
mod weapon;
mod armor;
mod consumable;
mod invitem;
pub use serialize::*;
pub use weapon::*;
pub use invitem::InvItem;
pub use armor::*;
pub use consumable::*;

use crate::stats::Attribute;

pub trait Item {
    fn name(&self) -> &'static str;
    fn value(&self) -> u32;
    fn description(&self) -> Option<&String>;
    fn requirement(&self) -> &Attribute;
    fn tooltip(&self) -> String;
    fn into_inv_item(self) -> InvItem;
}