use crate::stats::{
    Attribute, DamageType, MagicDamageType,
    PhysicalDamageType, Resistance,
}; // imports required stats types required by items.
use std::fmt::Write;
use crate::item::invitem::InvItem;
use super::{Item};

#[derive(Debug)]
/// holds information for a armor
pub struct Armor {
    pub name: &'static str,
    pub defense: i32,
    pub resistances: Option<Vec<Resistance>>,
    pub variant: ArmorVariant,
    pub description: Option<String>,
    pub value: u32,
    pub requirement: Attribute,
}

impl Armor {
    pub fn resistances_to_string(&self) -> Option<String> {
        if self.resistances.is_some() {
            let mut text = String::new();
            write!(text, "Resistances: ");
            for resistance in self.resistances.as_ref().unwrap() {
                write!(text, "\n{}", resistance.into_string());
            }
            return Some(text);
        } else {
            return None;
        }
    }
}

impl Item for Armor {
    fn name(&self) -> &'static str {
        &self.name()
    }
    fn value(&self) -> u32 {
        self.value
    }
    fn description(&self) -> Option<&String> {
        self.description.as_ref()
    }
    fn requirement(&self) -> &Attribute {
        &self.requirement
    }
    fn tooltip(&self) -> String {
        unimplemented!();
    }
    fn into_inv_item(self) -> InvItem {
        InvItem::Armor(self)
    }
}

#[derive(Debug)]
/// Different armor classifications for various amounts of defense.
pub enum ArmorVariant {
    Light,
    Medium,
    Heavy,
    VeryHeavy,
}

#[derive(Debug)]
/// Various kinds of armor used for end user display.
pub enum ArmorKind {
    FullPlate,
    ChainMail,
    Leather,
    Robe,
    Clothes,
}

impl ArmorKind {
    /// Assembles an Armor struct from an ArmorKind enum.
    pub fn into_armor(&self) -> Option<Armor> {
        match self {
            ArmorKind::FullPlate => Some(Armor {
                name: "Full Plate",
                defense: 50,
                resistances: Some(vec![Resistance::new(
                    32,
                    DamageType::Magic(MagicDamageType::Fire),
                )]),
                variant: ArmorVariant::VeryHeavy,
                description: Some("Tooltip".to_string()),
                value: 30,
                requirement: Attribute {
                    strength: Some(45),
                    dexterity: Some(25),
                    vitality: Some(30),
                    energy: None,
                    magic: None,
                },
            }),
            ArmorKind::ChainMail => Some(Armor {
                name: "Chain Mail",
                defense: 30,
                resistances: Some(vec![Resistance::new(
                    32,
                    DamageType::Magic(MagicDamageType::Fire),
                )]),
                variant: ArmorVariant::Heavy,
                description: Some("Tooltip".to_string()),
                value: 30,
                requirement: Attribute {
                    strength: Some(35),
                    dexterity: Some(15),
                    vitality: Some(10),
                    energy: None,
                    magic: None,
                },
            }),
            ArmorKind::Leather => Some(Armor {
                name: "Leather",
                defense: 15,
                resistances: Some(vec![Resistance::new(
                    32,
                    DamageType::Magic(MagicDamageType::Fire),
                )]),
                variant: ArmorVariant::Medium,
                description: Some("Tooltip".to_string()),
                value: 30,
                requirement: Attribute {
                    strength: Some(5),
                    dexterity: Some(10),
                    vitality: Some(5),
                    energy: None,
                    magic: None,
                },
            }),
            ArmorKind::Robe => Some(Armor {
                name: "Robe",
                defense: 5,
                resistances: Some(vec![
                    DamageType::Magic(MagicDamageType::Fire).into_resistance(25),
                    DamageType::Magic(MagicDamageType::Frost).into_resistance(25),
                    DamageType::Magic(MagicDamageType::Lightning).into_resistance(25),
                    DamageType::Magic(MagicDamageType::Light).into_resistance(15),
                    DamageType::Magic(MagicDamageType::Dark).into_resistance(15),
                    DamageType::Physical(PhysicalDamageType::Piercing).into_resistance(-25),
                    DamageType::Physical(PhysicalDamageType::Slashing).into_resistance(-25),
                    DamageType::Physical(PhysicalDamageType::Bludgeoning).into_resistance(-25),
                ]),
                variant: ArmorVariant::Light,
                description: Some("Tooltip".to_string()),
                value: 30,
                requirement: Attribute {
                    strength: None,
                    dexterity: None,
                    vitality: None,
                    energy: Some(10),
                    magic: Some(10),
                },
            }),
            ArmorKind::Clothes => Some(Armor {
                name: "Clothes",
                defense: 1,
                resistances: Some(vec![Resistance::new(
                    32,
                    DamageType::Magic(MagicDamageType::Fire),
                )]),
                variant: ArmorVariant::Light,
                description: Some("Tooltip".to_string()),
                value: 30,
                requirement: Attribute {
                    strength: None,
                    dexterity: None,
                    vitality: None,
                    energy: None,
                    magic: None,
                },
            }),
        }
    }
}