use crate::stats::{
    attribute_to_string, AttackPower, Attribute, DamageType,
    PhysicalDamageType, Rarity,
}; // imports required stats types required by items.
use std::fmt::Write;
use crate::item::invitem::InvItem;
use super::{Item};

#[derive(Debug)]
/// holds information for a weapon
pub struct Weapon {
    pub name: &'static str,
    pub rarity: Rarity,
    pub power: AttackPower,
    pub description: Option<String>,
    pub value: u32,
    pub requirement: Attribute,
}

impl Item for Weapon {
    fn name(&self) -> &'static str {
        &self.name
    }
    fn value(&self) -> u32 {
        self.value
    }
    fn description(&self) -> Option<&String> {
        self.description.as_ref()
    }
    fn requirement(&self) -> &Attribute {
        &self.requirement
    }
    fn tooltip(&self) -> String {
        let no_description = String::from("No Description.");
        let Weapon {
            name,
            rarity,
            value,
            power,
            description,
            requirement,
        } = self;
        let (min_damage, max_damage, damage_type) = (&power.min, &power.max, &power.damage_type);
        let description = description.as_ref().unwrap_or(&no_description);
        let requirements = attribute_to_string(&self.requirement);
        let mut text = String::new();
        write!(
            text,
            "{} ({:?})\n{} - {} {:?} Damage\nValue: {}",
            name, rarity, min_damage, max_damage, damage_type, value
        );
        if let Some(stat_req) = requirements {
            write!(text, "\n\n{}", stat_req);
        };
        write!(text, "\n\n{}", description);

        text
    }
    fn into_inv_item(self) -> InvItem {
        InvItem::Weapon(self)
    }
}

#[derive(Debug)]
/// Different weapon types used by entities
pub enum WeaponKind {
    Katana,
    Dagger,
}

impl WeaponKind {
    /// Assembles a Weapon struct from a WeaponKind enum.
    pub fn into_weapon(&self) -> Option<Weapon> {
        match self {
            WeaponKind::Katana => Some(Weapon {
                name: "Katana",
                rarity: Rarity::Rare,
                power: AttackPower { min: 5, max: 10, damage_type: DamageType::Physical(PhysicalDamageType::Piercing) },
                description: Some("This is a Japanese sword used by the samurai in ancient times. It's characterized by it's destinctive appearance: a curved, single-edged blade with a squared guard and a long grip to allow for two hands.".to_string()),
                value: 30,
                requirement: Attribute { strength: Some(15), dexterity: Some(8), vitality: None, energy: None, magic: None }
            }),
            WeaponKind::Dagger => Some(Weapon {
                name: "Dagger",
                rarity: Rarity::Common,
                power: AttackPower { min: 2, max: 6, damage_type: DamageType::Physical(PhysicalDamageType::Piercing) },
                description: Some("This is a dagger, such descriptive".to_string()),
                value: 30,
                requirement: Attribute { strength: None, dexterity: None, vitality: None, energy: None, magic: None }
            }),
        }
    }
}