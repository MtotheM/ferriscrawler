use super::Weapon;
use super::Armor;
use super::Consumable;
use super::Item;

#[derive(Debug)]
pub enum InvItem {
    Weapon(Weapon),
    Armor(Armor),
    Consumable(Consumable),
}

impl InvItem {
    pub fn name(&self) -> &'static str {
        match self {
            InvItem::Weapon(weapon) => &weapon.name,
            InvItem::Armor(armor) => &armor.name,
            InvItem::Consumable(consumable) => &consumable.name,
        }
    }
    pub fn description(&self) -> Option<&String> {
        match self {
            InvItem::Weapon(weapon) => weapon.description.as_ref(),
            InvItem::Armor(armor) => armor.description.as_ref(),
            InvItem::Consumable(consumable) => None,
        }
    }
    pub fn tooltip(&self) -> String {
        /// takes an attribute stats and formats a string listing stat requirements if present.
        let no_description = String::from("No Description.");
        match self {
            InvItem::Weapon(weapon) => weapon.tooltip(),
            InvItem::Armor(armor) => return format!("{}\n{}", armor.name, armor.defense),
            InvItem::Consumable(consumable) => unimplemented!(),
        }
    }
    /// value when an item is sold, 75% of buy value.
    pub fn value_sell(&self) -> u32 {
        match self {
            InvItem::Weapon(weapon) => (weapon.value as f64 * 0.75) as u32,
            InvItem::Armor(armor) => (armor.value as f64 * 0.75) as u32,
            InvItem::Consumable(consumable) => 0,
        }
    }
    /// value when an item is bought.
    pub fn value_buy(&self) -> u32 {
        match self {
            InvItem::Weapon(weapon) => weapon.value,
            InvItem::Armor(armor) => armor.value,
            InvItem::Consumable(consumable) => 0,
        }
    }
    pub fn equip(&self) {
        unimplemented!();
    }
}