use crate::stats::{Effect};

#[derive(Debug)]
/// holds information for a consumable.
pub struct Consumable {
    pub name: &'static str,
    pub kind: ConsumableKind,
    pub effect: Effect,
}

#[derive(Debug)]
/// Various kinds of consumables (WiP).
pub enum ConsumableKind {
    Potion(PotionKind),
    Food(FoodKind),
}

#[derive(Debug)]
/// Various kinds of potion used for end user display (WiP).
pub enum PotionKind {
    Healing,
    Poison,
}

#[derive(Debug)]
/// Various kinds of food used for end user display (WiP).
pub enum FoodKind {
    Healing,
}

impl Consumable {
    /// Assembles a Consumable struct from a PotionKind enum (WiP).
    fn from(kind: ConsumableKind) -> Consumable {
        match kind {
            ConsumableKind::Potion(PotionKind::Healing) => Consumable {
                name: "Healing Potion",
                kind: ConsumableKind::Potion(PotionKind::Healing),
                effect: Effect::Heal(15),
            },
            ConsumableKind::Potion(PotionKind::Poison) => Consumable {
                name: "Posion Potion",
                kind: ConsumableKind::Potion(PotionKind::Poison),
                effect: Effect::Damage(50),
            },
            ConsumableKind::Food(FoodKind::Healing) => Consumable {
                name: "Hotdog",
                kind: ConsumableKind::Food(FoodKind::Healing),
                effect: Effect::Heal(2),
            },
        }
    }
}