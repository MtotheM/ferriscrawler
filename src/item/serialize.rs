use crate::item::{Weapon, Armor, Consumable};

use serde_derive::{Serialize, Deserialize};
use serde_json::json;

use std::collections::HashMap;

//#[derive(Deserialize, Serialize)]
pub struct Items {
    pub weapons: HashMap<u32, Weapon>,
    pub armors: HashMap<u32, Armor>,
    pub consumable: HashMap<u32, Consumable>,
}