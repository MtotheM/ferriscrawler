use rand::Rng; // imports the rand crate, used for randomizing choices and numbers.

#[derive(Debug)]
/// holds a positive amount of money.
pub struct Money {
    amount: u32,
}

impl Money {
    /// creates a new money struct with a specified amount.
    pub fn new(amount: u32) -> Money {
        Money { amount }
    }
    /// creates a new money struct with a random amount between two specified values.
    pub fn new_range(low: u32, high: u32) -> Money {
        let amount = rand::thread_rng().gen_range(low, high + 1);
        Money { amount }
    }
    // will overflow, fix
    pub fn add(&mut self, amount: u32) {
        self.amount += amount;
    }
    /// subtracts an amount of money, and returns how much were lost, can't go negative.
    pub fn sub(&mut self, amount: u32) -> u32 {
        let old_amount = self.amount;
        self.amount = old_amount.saturating_sub(amount);

        old_amount - self.amount
    }
    /// cuts the current amount of money in half, and returns how much were lost.
    pub fn cut_in_half(&mut self) -> u32 {
        let old_amount = self.amount;
        self.amount /= 2;

        old_amount - self.amount
    }
}