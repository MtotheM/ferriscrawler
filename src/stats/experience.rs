#[derive(Debug)]
/// holds a positive amount of experience.
pub struct Experience {
    amount: u32,
}