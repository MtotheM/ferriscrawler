#[derive(Debug)]
pub enum Rarity {
    Common,
    Rare,
    Artifact,
}