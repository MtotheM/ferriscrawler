use std::fmt; // ??? (WiP).
use crate::stats::{DamageEffect, HealEffect};

#[derive(Debug)]
pub struct Health {
    pub current: i32,
    pub max: i32,
}
impl Health {
    pub fn new(maximum: i32) -> Health {
        Health {
            current: maximum,
            max: maximum,
        }
    }
    pub fn restore(&mut self) -> HealEffect {
        let restored = self.max - self.current;
        self.current = self.max;

        HealEffect {
            amount: restored,
            overheal: 0,
        }
    }
    pub fn drain(&mut self) -> DamageEffect {
        let drained = self.current;
        self.current = 0;

        DamageEffect {
            amount: drained,
            overkill: 0,
        }
    }
    pub fn add(&mut self, amount: i32) -> HealEffect {
        let new = self.current + amount;
        let capped = self.max.min(new);
        let healing = capped - self.current;
        let overhealing = new - self.max;

        self.current = capped;
        HealEffect {
            amount: healing,
            overheal: overhealing,
        }
    }
    pub fn sub(&mut self, amount: i32) -> DamageEffect {
        let mut old_health = self.current;
        let mut new_health = old_health - amount;
        let mut overkill: i32 = 0;
        let mut damage: i32 = 0;

        if new_health < 0 {
            damage += old_health;
            overkill += new_health.abs();
            self.current = 0;
        } else {
            damage = old_health - new_health;
            self.current = new_health;
        }
        DamageEffect {
            amount: damage,
            overkill,
        }

        //            let mut damage = self.current - amount;
        //            let mut overkill = 0;
        //            if damage < 0 {
        //                overkill = damage.abs();
        //                damage = self.current;
        //            }
        //            self.current -= damage;
        //            DamageEffect { amount: damage, overkill }
    }
}
impl fmt::Display for Health {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} / {}", self.current, self.max)?;
        if self.current <= 0 {
            write!(f, " (dead)")?;
        }
        Ok(())
    }
}