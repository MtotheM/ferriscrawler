#[derive(Debug)]
pub enum Effect {
    Heal(i32),
    Damage(i32),
    Regen(i32),
}
#[derive(Debug)]
pub struct HealEffect {
    pub amount: i32,
    pub overheal: i32,
}
#[derive(Debug)]
pub struct DamageEffect {
    pub amount: i32,
    pub overkill: i32,
}
#[derive(Debug)]
pub struct RegenEffect {
    pub amount: i32,
    pub ticks: i32,
    pub counter: i32,
}