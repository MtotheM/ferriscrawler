use super::DamageType;

#[derive(Debug)]
/// holds the resistance intensity along with it's damage type.
pub struct Resistance {
    pub intensity: i32,
    pub damage_type: DamageType,
}

impl Resistance {
    /// assembles a new Resistance struct based on intensity and damage type.
    pub fn new(intensity: i32, damage_type: DamageType) -> Resistance {
        Resistance {
            intensity,
            damage_type,
        }
    }
    /// returns a floating point number respresenting the intensity of the resistance.
    pub fn value(&self) -> i32 {
        self.intensity
    }
    /// returns a refference to the resistance damage type.
    pub fn element(&self) -> &DamageType {
        &self.damage_type
    }
    /// constructs a string based on the element.
    pub fn element_to_string(&self) -> &'static str {
        self.element().inner_to_string()
    }
    /// constructs a string formatted as (+25 Fire / -50 Piercing).
    pub fn into_string(&self) -> String {
        let mut operator = String::new();
        if self.intensity > 0 {
            String::new();
        }
        format!("{}{}% {}", operator, self.value(), self.element_to_string())
    }
}

//    impl ResistanceIntensity {
//        /// Assembles a Resistance struct based on the intensity and damage type.
//        pub fn to(self, damage_type: DamageType) -> Resistance {
//            Resistance {
//                intensity: self,
//                damage_type,
//            }
//        }
//        /// returns a floating point number respresenting the resistance intensity.
//        pub fn as_value(&self) -> f32 {
//            match self {
//                ResistanceIntensity::Immune => 0.0,
//                ResistanceIntensity::VeryResistant => 0.50,
//                ResistanceIntensity::Resistant => 0.25,
//                ResistanceIntensity::Weak => 1.25,
//                ResistanceIntensity::VeryWeak => 1.5,
//                ResistanceIntensity::SuperWeak => 2.0,
//            }
//        }
//    }