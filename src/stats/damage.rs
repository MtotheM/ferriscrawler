use rand::Rng; // imports the rand crate, used for randomizing choices and numbers.
use crate::stats::Resistance;

#[derive(Debug)]
/// holds various damage types.
pub enum DamageType {
    Physical(PhysicalDamageType),
    Magic(MagicDamageType),
}

impl DamageType {
    pub fn outer_to_string(&self) -> &'static str {
        match self {
            DamageType::Physical(PhysicalDamageType) => "Physical",
            DamageType::Magic(MagicDamageType) => "Magic",
        }
    }
    pub fn inner_to_string(&self) -> &'static str {
        match self {
            DamageType::Physical(PhysicalDamageType) => match PhysicalDamageType {
                PhysicalDamageType::Piercing => "Piercing",
                PhysicalDamageType::Slashing => "Slashing",
                PhysicalDamageType::Bludgeoning => "Bludgeoning",
            },
            DamageType::Magic(MagicDamageType) => match MagicDamageType {
                MagicDamageType::Fire => "Fire",
                MagicDamageType::Frost => "Frost",
                MagicDamageType::Lightning => "Lightning",
                MagicDamageType::Light => "Light",
                MagicDamageType::Dark => "Dark",
            },
        }
    }
    pub fn into_resistance(self, intensity: i32) -> Resistance {
        Resistance::new(intensity, self)
    }
}

#[derive(Debug)]
pub enum PhysicalDamageType {
    Piercing,
    Slashing,
    Bludgeoning,
}

#[derive(Debug)]
pub enum MagicDamageType {
    Fire,
    Frost,
    Lightning,
    Light,
    Dark,
}

#[derive(Debug)]
/// holds various attack ratings, such as critial hits or glancing blows.
pub enum AttackRating {
    Miss,
    Glance,
    Hit,
    CritialHit,
}

#[derive(Debug)]
/// holds the damage amount along with it's attack rating.
pub struct Damage {
    pub amount: u32,
    pub rating: AttackRating,
}

#[derive(Debug)]
/// holds the range of damage something can inflict.
pub struct AttackPower {
    pub min: u32,
    pub max: u32,
    pub damage_type: DamageType,
}

impl AttackPower {
    /// yields a damage struct based on attack power and returns a damage struct.
    pub fn gen_damage(&self) -> Damage {
        let amount = rand::thread_rng().gen_range(self.min, self.max + 1);
        let percent = (amount / self.max) as f32;

        let rating = if percent >= 0.9 {
            AttackRating::CritialHit
        } else if percent >= 0.3 {
            AttackRating::Hit
        } else if percent > 0.0 && percent < 0.3 {
            AttackRating::Glance
        } else {
            AttackRating::Miss
        };
        Damage { amount, rating }
    }
}