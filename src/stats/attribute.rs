use std::fmt::Write;

/// compares two attributes, if base has sufficent stats compared to anothet attribute, returns true.
pub fn attribute_is_sufficent(base_atr: &Attribute, compare_atr: &Attribute) -> bool {
    // FIX ME TO RETURN A TUPLE
    let stat_names = ["strength", "dexterity", "vitality", "energy", "magic"];
    let base_values = [
        base_atr.strength,
        base_atr.dexterity,
        base_atr.vitality,
        base_atr.energy,
        base_atr.magic,
    ];
    let compare_values = [
        compare_atr.strength,
        compare_atr.dexterity,
        compare_atr.vitality,
        compare_atr.energy,
        compare_atr.magic,
    ];
    for (base, compare) in Iterator::zip(base_values.iter(), compare_values.iter()) {
        if base < compare {
            return false;
        }
    }
    true
}

/// converts attributes to a string if any attribute is present. else returns None.
pub fn attribute_to_string(atr: &Attribute) -> Option<String> {
    let stat_values = [
        atr.strength,
        atr.dexterity,
        atr.vitality,
        atr.energy,
        atr.magic,
    ];
    let stat_names = ["strength", "dexterity", "vitality", "energy", "magic"];
    let mut text = String::new();
    for (opt_stat_value, stat_name) in Iterator::zip(stat_values.iter(), stat_names.iter()) {
        if let Some(stat_value) = opt_stat_value {
            write!(text, "\n{} {}", stat_value, stat_name);
        }
    }
    if text.len() > 0 {
        Some(format!("Requirements: {}", text))
    } else {
        None
    }
}

#[derive(Debug)]
/// holds attributes related to items and entities.
pub struct Attribute {
    pub strength: Option<u32>,
    pub dexterity: Option<u32>,
    pub vitality: Option<u32>,
    pub energy: Option<u32>,
    pub magic: Option<u32>,
}

impl Attribute {
    /// creates a new attribute struct where all stats are the same.
    pub fn new_base(base: Option<u32>) -> Attribute {
        //let base = if base > 0 { Some(base) } else { None };
        Attribute {
            strength: base,
            dexterity: base,
            vitality: base,
            energy: base,
            magic: base,
        }
    }
    /// creates a new attribute struct where each stat is set manually.
    pub fn new(
        strength: Option<u32>,
        dexterity: Option<u32>,
        vitality: Option<u32>,
        energy: Option<u32>,
        magic: Option<u32>,
    ) -> Attribute {
        Attribute {
            strength,
            dexterity,
            vitality,
            energy,
            magic,
        }
    }
    pub fn gen_level(&self) -> u32 {
        let base = 5;
        let mut level = 1;
        let stats = [
            self.strength,
            self.dexterity,
            self.vitality,
            self.energy,
            self.magic,
        ];
        for stat in stats.iter() {
            let stat = stat.unwrap_or(0);
            if stat > 5 {
                level += stat - base
            }
        }
        level
    }
}