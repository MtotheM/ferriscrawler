pub struct UserInput {
    name: &'static str,
    class: &'static str,
    race: &'static str,
}
