use crate::stats;
use rand::Rng;

///Returns a bool based on probability, can be between 0-100+ (anything above or equal to 100 is guaranteed to be true)
pub fn roll(probability: u32) -> bool {
    probability >= rand::thread_rng().gen_range(0, 100 + 1)
}
pub fn hit(probability: u32) -> bool {
    roll(probability)
}
pub fn drop(probability: u32) -> bool {
    roll(probability)
}
pub fn damage(ap: stats::AttackPower) -> u32 {
    rand::thread_rng().gen_range(ap.min, ap.max + 1)
}
